#!/usr/bin/env python3
import json
from random import randint
import sys
import urllib.request as request


def get_response(url):
    return request.urlopen(request.Request(url)).read()


def response_to_json(response):
    return json.loads(response.decode('utf-8'))


def find_actual_xkcd_number():
    url = "https://xkcd.com/info.0.json"
    return response_to_json(get_response(url)).get('num')


def random_xkcd_num():
    return randint(1, find_actual_xkcd_number())


def get_xkcd_alt_text(num):
    url = "https://xkcd.com/{0}/info.0.json".format(num)
    return response_to_json(get_response(url)).get('alt')


def build_bio(name, l_act, d_act, pron, bad_hab, r_act, c_act, exc, pol):
    base_string = """
{name} prefers {like_action} than {dislike_action}.
{pronoun} is that kind of coder that {bad_habit}.
As XKCD #{xkcd_num} states {name}'s code seems like
'{xkcd_alt}'. {pronoun} usually relaxes {relax_action}.
When {pronoun}'s concentrated {name} likes to write about
{conc_action} on its blog that is never updated. Recently,
friends saw {name} with a lighter that says -'You sleep
little and you know it'-. Maybe due to {excuse}. Somebody
tell {pronoun} has politically {politics} opinions but we
are not sure.
    """

    xkcd_num = random_xkcd_num()
    xkcd_alt = get_xkcd_alt_text(xkcd_num)
    
    return base_string.format(
        name=name,
        pronoun=pron,
        like_action=l_act,
        dislike_action=d_act,
        bad_habit=bad_hab,
        xkcd_num=xkcd_num,
        xkcd_alt=xkcd_alt,
        relax_action=r_act,
        conc_action=c_act,
        excuse=exc,
        politics=pol,
    ).replace("\n", " ")


def main():
    sys.stdout.write("Welcome to Pretty Good Biography. ")
    sys.stdout.write("You're closer to build your bio.")
    sys.stdout.write("\n")
    sys.stdout.write("Please, type your nickname or name:\n")
    name = str(input('---> '))
    sys.stdout.write("Please, type 0 if you want to be referred as 'she', ")
    sys.stdout.write("or 1 if you want to be referred as 'he'\n")
    sys.stdout.write("For misgenders submit a patch please.\n")
    she_or_he_dict = {0: "She", 1: "He"}
    pron = she_or_he_dict[int(input('---> '))]
    sys.stdout.write("Please, type a whole action that you like to do e.g.: ")
    sys.stdout.write("'swimming in the bath full of ice cubes'.\n")
    l_act = str(input('---> '))
    sys.stdout.write("Please, type a whole action that you dislike to do ")
    sys.stdout.write("e.g.: 'watching the skaters doing tricks while i'm ")
    sys.stdout.write("eating ice-cream'.\n")
    d_act = str(input('---> '))
    sys.stdout.write("Please, type a bad coding habit that you would like ")
    sys.stdout.write("to improve. E.g.: 'never put a comment on variable ")
    sys.stdout.write("declaration'. \n")
    bad_hab = str(input('---> '))
    sys.stdout.write("Please, type a whole action that you do when you're ")
    sys.stdout.write("relaxing. E.g.: 'listening to hardcore music in front ")
    sys.stdout.write("of two mirrors'. \n")
    r_act = str(input('---> '))
    sys.stdout.write("Please, type an action that you do when you're ")
    sys.stdout.write("concentrated. E.g.: 'separing the good grapes from the")
    sys.stdout.write(" bad apples'. \n")
    c_act = str(input('---> '))
    sys.stdout.write("Please, type your favorite excuse. E.g.: 'only hear ")
    sys.stdout.write("instead of listening'. \n")
    exc = str(input('---> '))
    sys.stdout.write("Please, type 0 if you feel conservative, 1 if moderate,")
    sys.stdout.write("2 if radical.\n")
    politics_dict = {0: "conservative", 1: "moderate", 2: "radical"}
    pol = politics_dict[int(input('---> '))]
    sys.stdout.write("Thx for your patience... And your flamant bio is:\n\n")
    sys.stdout.write(
        build_bio(name, l_act, d_act, pron, bad_hab, r_act, c_act, exc, pol)
    )
    sys.stdout.write("\n\n")


if __name__ == '__main__':
    main()
